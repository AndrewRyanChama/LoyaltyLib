import sqlite3
import time

from flask import Flask, g, request, redirect, url_for
from flask_basicauth import BasicAuth

app = Flask(__name__)

app.config['BASIC_AUTH_USERNAME'] = 'john'
app.config['BASIC_AUTH_PASSWORD'] = 'matrix'

basic_auth = BasicAuth(app)

DATABASE = 'loyalty.db'

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
        db.row_factory = sqlite3.Row
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

@app.route('/')
def hello():
    return 'Helloworld'

@app.route('/reds')
@basic_auth.required
def reds():
    cur = get_db().cursor()
    # Insert a row of data
    cur.execute("select * from red_chat where valid_until > ?", (int(time.time() * 1000),))
    result = cur.fetchall()
    # print result[0].keys()a
    # print result
    converted = [{"chatId": row["chat_id"], "listenerName": row["listener_name"], "message": row["message"], "validUntil": row["valid_until"], "color": row["color"]} for row in result]
    # print converted
    return {"result": converted}

@app.route('/handle_data', methods=['POST'])
@basic_auth.required
def handle_data():
    # projectpath = request.form['projectFilepath']
    print request.form
    cur = get_db().cursor()
    cur.execute("insert into red_chat (chat_id, listener_name, message, valid_until, color) values (?, ?, ?, ?, ?)", (request.form["chatId"], request.form["name"], request.form["message"], request.form["validUntil"], request.form["color"]))
    get_db().commit()
    return redirect(url_for('static', filename='debugform.html'))

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.

if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
